﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace oppexam25102020
{
    class Program
    {
        static void Main(string[] args)
        {
            // part 3 q 1 
            List<string> aviList = new List<string>()
            {
                "mama",
                "lama",
                "kacha",
                "apple",
                "sumsong",
                "iphone pro 12",
                "iphone pro 11",
                "not an iphone"
            };
            var avi = new Part3Q1(aviList);
            avi.StartGame();


            // part 3 q 2 
            Rational a = new Rational(4, 5);
            Rational b = new Rational(4, 5);
            Rational c = new Rational(3, 5);

            Console.WriteLine(a == b);
            Console.WriteLine(a > c);
            Console.WriteLine(a < c);
            Console.WriteLine(a + b);
            Console.WriteLine(a + c);
            Console.WriteLine(a - c);
            Console.WriteLine(a - b);
            Console.WriteLine(a * b);

        }
    }
}
