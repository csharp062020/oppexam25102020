﻿using System;
using System.Collections.Generic;

namespace oppexam25102020
{
    class MyUniqueList
    {
        List<int> list = new List<int>();

        public MyUniqueList()
        {

        }

        public bool Add(int item)
        {
            if (!list.Contains(item))
            {
                list.Add(item);
                return true;
            }
            throw new ItemAlreadyExistException();

            return false;
        }

        public bool Remove(int item)
        {
            if (list.Contains(item))
            {
                list.Remove(item);
                return true;
            }

            throw new ItemNotFoundException();
            return false;
        }

        public int Peek(int index)
        {
            return list[index];
        }

        public int this[int index]
        {
            get
            {
                if (!(index > (list.Count - 1)))
                {
                    return this.list[index];
                }
                throw new IndexOutOfRangeException();
            }
            set
            {
                if (list[index] == value)
                    throw new ItemAlreadyExistException();
                //return;
                if (list.Contains(value))
                    throw new ItemAlreadyExistException();
                return;
                list[index] = value;
            }
        }
    }
}