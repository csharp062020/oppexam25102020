﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oppexam25102020
{
    public class Part3Q1
    {
        private String _word;
        private String _wordGessString;
        private StringBuilder _wordGess;
        private List<string> _wordList;

        public Part3Q1(List<string> wordList)
        {
            _wordList = wordList;
            SetUp();

        }

        //if we want anther ctor with a one word only
        //public Part3Q1(string word)
        //{
        //    SetUp(word);
        //}

        private void SetUp(string word)
        {
            _word = word;
            _wordGessString = new String('_', _word.Length);
            _wordGess = new StringBuilder(_wordGessString);
        }

        private void SetUp()
        {
            SetUp(RandomWordFromList());
        }
        private string RandomWordFromList()
        {
            Random rnd = new Random();
            int wordNumber = rnd.Next(0, _wordList.Count - 1);
            return _wordList[wordNumber];

        }
        private void ReplaceUnderSpaceCharToLetter(char letter)
        {
            int i = 0;
            string lowerLetter = letter.ToString().ToLower();
            while (_word.ToLower().IndexOf(lowerLetter, i) != -1)
            {
                i = _word.ToLower().IndexOf(lowerLetter, i);
                _wordGess[i] = _word[i];
                i++;
            }
        }

        private void WordStatus()
        {
            Console.WriteLine();
            for (int i = 0; i < _wordGess.Length; i++)
            {
                Console.Write($"{_wordGess[i]} ");
            }

            Console.WriteLine();
        }
        private void WordStatus(int guess)
        {
            Console.WriteLine($"------ guess number {guess} ------");
            WordStatus();
        }
        public void StartGame()
        {
            int gameNumber = 1;
            bool game = true;
            while (game)
            {
                int numberOfGess = 0;
                GameHeadLines(gameNumber);
                numberOfGess = MainGameGuess(numberOfGess);
                game = EndOfGameAfterGuess(numberOfGess);
            }
        }

        private int MainGameGuess(int numberOfGess)
        {
            while (_wordGess.ToString().Contains('_'))
            {
                Char.TryParse(Console.ReadLine(), out char a);
                numberOfGess++;
                ReplaceUnderSpaceCharToLetter(a);
                WordStatus(numberOfGess);
            }

            return numberOfGess;
        }

        private bool EndOfGameAfterGuess(int numberOfGess)
        {
            Console.WriteLine($"Ok you succeeded to solve this in {numberOfGess} guesses ");

            Console.WriteLine();
            Console.WriteLine("do you want to continue play ? (1 to exit , other char to continue) ");

            int.TryParse(Console.ReadLine(), out int result);
            if (result == 1)
            {
                Console.WriteLine("Hope you enjoy by by !! , see you soon (or not) ");
                return false;
            }
            else
            {
                SetUp();
                return true;
            }

        }

        private void GameHeadLines(int gameNumber)
        {
            Console.WriteLine($"---------- Game Number {gameNumber} ----------");
            Console.WriteLine($"you have to guess a word with {_wordGess.Length}  chars , good luck");
            Console.WriteLine(
                "please enter 1 char / letter every time , more then one letter/char will count as guess but no ack done with it.");
            WordStatus();
        }
    }
}