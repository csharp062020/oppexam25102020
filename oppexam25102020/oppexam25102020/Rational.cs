﻿using System;

namespace oppexam25102020
{
    class Rational
    {
        private int _mone;
        private int _mechane;

        public Rational(double mone, double mechane)
        {


            if (mone % 1 == 0.0 && mechane > 0)
            {
                Int32.TryParse(mone.ToString(), out _mone);
                Int32.TryParse(mechane.ToString(), out _mechane);

            }
            else
            {
                _mone = 0;
                _mechane = 0;
            }

        }

        public int GetNumerator()
        {
            return _mone;
        }

        public int GetDenomirator()
        {
            return _mechane;
        }

        public override string ToString()
        {
            return $"{_mone}/{_mechane}";
        }

        private static double NumberAfterKefel(Rational aRational)
        {
            return (aRational._mone * aRational._mechane);
        }
        public static bool operator >(Rational aRational, Rational bRational)
        {
            IsNull(aRational, bRational);


            return NumberAfterKefel(aRational) > NumberAfterKefel(bRational);
        }

        public static bool operator <(Rational aRational, Rational bRational)
        {
            IsNull(aRational, bRational);

            return NumberAfterKefel(aRational) < NumberAfterKefel(bRational);
        }

        private static void IsNull(Rational aRational, Rational bRational)
        {
            if (aRational is null ||
                bRational is null)
                throw new ArgumentNullException();
        }

        public static bool operator ==(Rational aRational, Rational bRational)
        {
            IsNull(aRational, bRational);
            return Math.Abs(NumberAfterKefel(aRational) - NumberAfterKefel(bRational)) < 0.1;
        }

        public static bool operator !=(Rational aRational, Rational bRational)
        {
            return !(aRational == bRational);
        }

        public override bool Equals(object obj)
        {
            return this == obj as Rational;
        }

        public static Rational operator *(Rational aRational, Rational bRational)
        {
            IsNull(aRational, bRational);
            return new Rational(aRational._mone * bRational._mone, aRational._mechane * bRational._mechane);
        }

        public static Rational operator +(Rational a, Rational b)
        {
            IsNull(a, b);
            return new Rational(a._mone * b._mechane + b._mone * a._mechane, a._mechane * b._mechane);
        }
        public static Rational operator -(Rational a, Rational b)
        {
            IsNull(a, b);
            return new Rational(a._mone * b._mechane - b._mone * a._mechane, a._mechane * b._mechane);
        }
    }
}